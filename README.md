# Currency Converter

## About

it's a currency converter app that exchange rates available countries. It shows you the mid-market live rate.

## you can try this app from the link below

<h2>
<a href="https://oneeyex.github.io/Currency-Converter/" target="_blank"> Link to try Currency Converter</a></h2>

## NB this app is a beta version

<h3> this app is created by <a href="https://www.linkedin.com/in/chedly-chahed-a178a9196/"> Chedly Chahed</a> </h3>

###

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or Contact me.
